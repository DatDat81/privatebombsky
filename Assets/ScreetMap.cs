﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreetMap : MonoBehaviour
{
    
    public List<GameObject> map;
    public BoxCollider2D box;

    [Header("Not Default Mode, Trap Mode")]
    public bool modeTrap;
    /// <summary>
    /// có 2 kiểu.
    /// 1. default: chỉ làm map ẩn cuất hiện
    /// 2. không phải defaultmode: làm cho map ẩn+ enemy trong map ẩn xuất hiện(một dạng bẫy đóng kín) khi nào đánh được enemy thì mới thoát được map ẩn(trap)
    /// </summary>
    bool notHide =false;// kiểm tra xem enemy đã hiện chưa
    public List<GameObject> enemy;

    [Header("Hide Somethings")]
    public bool haveSomethingHide;
    public List<GameObject> objHide;

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.tag=="Player")
        {
            for(int i=0;i<map.Count;i++)
            {
                map[i].SetActive(true);
            }
            if(haveSomethingHide==true) // neu co obj hide
            {
                for (int i = 0; i < objHide.Count; i++)
                {
                    objHide[i].SetActive(false);
                }
            }
            if (modeTrap==true)
            {
                for (int i = 0; i < enemy.Count; i++)
                {
                    enemy[i].SetActive(true);
                }
                notHide = true;
            }
            box.enabled = false;
        }
    }
    private void Update()
    {
        if(modeTrap==true)
        {
            if(notHide==true)
            {
                for (int i = 0; i < enemy.Count; i++)
                {
                    if(enemy[i].activeSelf==false)
                    {
                        enemy.RemoveAt(i);
                    }
                }
                if(enemy.Count==0)
                {
                    for (int i = 0; i < map.Count; i++)
                    {
                        map[i].SetActive(false);
                    }
                    gameObject.SetActive(false);
                }
            }
        }
    }
}

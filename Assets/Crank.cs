﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crank : MonoBehaviour
{
    public List<GameObject> obj; // khi crank up thi obj này sẽ xuất hiện 
    public Animator ani;
    bool crankup = false;
    public bool hide; // mode ẩn , hiện

    public void CrankUp()
    {
        if(crankup==false)
        {
            ani.SetBool("CrankUp", true);
            if(hide==false) // hiện obj
            {
                for (int i = 0; i < obj.Count; i++)
                {
                    obj[i].SetActive(true);
                }
            }
            else// ẩn obj
            {
                for (int i = 0; i < obj.Count; i++)
                {
                    obj[i].SetActive(false);
                }
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour
{
    public int distanceLife;
    public float speed;

    public bool move = false;
    Vector2 direction;
    GameObject obj;

    void Update()
    {
        if(move==true)
        {
            SetActiver();
            transform.Translate(direction * speed*Time.deltaTime);
        }

    }

    public void SetActiver()
    {
        if(Vector2.Distance(transform.position,obj.transform.position)>=distanceLife)
        {
            gameObject.SetActive(false);
        }
    }

    public void SetDirection_GameObject(Vector2 dir, GameObject gameObject)
    {
        direction = dir;
        obj = gameObject;
    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.tag=="Player")
        {
            if(coll.GetComponent<Player>().GetDiePlayer()==false)
            {
                float x = coll.transform.position.x;
                float y = coll.transform.position.y;

                GameManager.singleton.ShakeCamera();
                GameObject explotion = ObjectPool.singleton.GetExplosion();
                explotion.transform.position = this.transform.position;
                explotion.SetActive(true);

                coll.GetComponent<Rigidbody2D>().velocity = new Vector2(-transform.position.x + x, -transform.position.y + y) * 4;
                //coll.GetComponent<Player>().SetDiePlayer();
                coll.GetComponent<Player>().HitPlayer();

                SoundManager.singleton.PlayExplosionSound();
                this.gameObject.SetActive(false);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    [Header("Direction")]
    public Transform pos1;
    public Transform pos2;
    public Transform posSpawn;

    [Header("other")]
    public float timeLoop;
    public Animator ani;

    void Start()
    {
        StartCoroutine("Shoot");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    GameObject obj;
    IEnumerator Shoot()
    {
        yield return new WaitForSeconds(timeLoop);
        while(true)
        {
            obj = ObjectPool.singleton.GetCannonBall();
            if(obj!=null)
            {
                ani.SetTrigger("Shoot");
                Invoke("Spawn", 0.3f);
            }
            yield return new WaitForSeconds(timeLoop);
        }
    }

    void Spawn()
    {
        //GameManager.singleton.ShakeCamera();
        obj.transform.position = posSpawn.position;
        obj.SetActive(true);
        obj.GetComponent<CannonBall>().SetDirection_GameObject(pos2.position - pos1.position, this.gameObject);
        obj.GetComponent<CannonBall>().move = true;
    }
}

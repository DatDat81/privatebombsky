﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    public float timeCountDown;
    private float counter;
    public bool startCount;
    public Rigidbody2D rb;

    public Animator ani;
    public bool bombOn = false;
    public float rad;
    public LayerMask whatIsObject;
    public Transform pointAddForce;
    public Transform pointCenter;

    private void OnEnable()
    {
        rb.bodyType = RigidbodyType2D.Dynamic;
        counter = timeCountDown;
    }

    void Start()
    {
        
        
    }
    public float force;
    void Update()
    {
        ani.SetBool("BombOn", bombOn);
        if(startCount==true)
        {
            if(counter<=0) // no bom
            {
                counter = timeCountDown;
                GameManager.singleton.ShakeCamera();

                GameObject explotion = ObjectPool.singleton.GetExplosion();
                explotion.transform.position = this.transform.position;
                explotion.SetActive(true);

                Collider2D[] colliders = Physics2D.OverlapCircleAll(pointCenter.position, rad, whatIsObject); // xet va cham
                foreach (Collider2D collider in colliders)
                {
                    float x = collider.transform.position.x;
                    float y = collider.transform.position.y;
                    if (collider.tag=="Object")
                    {                       
                        collider.GetComponent<Rigidbody2D>().velocity = new Vector2(-pointAddForce.position.x + x, -pointAddForce.position.y + y) * force / 2;
                    }
                    else if (collider.tag == "Crank")
                    {
                        collider.GetComponent<Crank>().CrankUp();
                    }
                    else 
                    {
                        collider.GetComponent<Rigidbody2D>().velocity = new Vector2(-pointAddForce.position.x + x, -pointAddForce.position.y + y) * force;
                        if(collider.tag=="Player")
                        {
                            //collider.GetComponent<Player>().SetDiePlayer();
                            collider.GetComponent<Player>().HitPlayer();
                        }
                        else if (collider.tag == "BaldPirate")
                        {
                            collider.GetComponent<BaldPirate>().SetDieEnemy();
                        }
                        else if (collider.tag == "Cucumber")
                        {
                            collider.GetComponent<Cucumber>().SetDieEnemy();
                        }
                        else if(collider.tag == "BigGuy")
                        {
                            collider.GetComponent<BigGuy>().SetDieEnemy();
                        }
                        else if (collider.tag == "Captain")
                        {
                            collider.GetComponent<Captain>().SetDieEnemy();
                        }
                        else if (collider.tag == "Whale")
                        {
                            collider.GetComponent<Whale>().SetDieEnemy();
                        }
                    }

                    
                }
                //rb.velocity = Vector2.zero;
                SoundManager.singleton.PlayExplosionSound();
                this.gameObject.SetActive(false);
                
            }
            else
            {
                counter -= Time.deltaTime;
            }
        }
    }

    public void BombOff()
    {
        bombOn = false;
        Invoke("Activer", 1f);
        startCount = false;
    }

    private void Activer()
    {
        gameObject.SetActive(false);
    }
}

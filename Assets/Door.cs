﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public GameObject door1;
    public GameObject door2;
    private Animator ani;
    bool countTime=false;
    public float time = 0.5f;
    float counter;
     
    void Start()
    {
        ani = GetComponent<Animator>();
        counter = time;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.tag=="Player")
        {
            ani.SetBool("OpenDoor", true);
            Player.singleton.SetAnimationDoorOut();
            if (Vector2.Distance(coll.transform.position, transform.position) >0.1)
            {
                Debug.Log("ha");
                if (counter <= 0)
                {
                    
                }
                else
                {
                    counter -= Time.deltaTime;
                }
                if (gameObject.name == "Door1")
                {
                    coll.transform.position = door2.transform.position;
                }
                else if (gameObject.name == "Door2")
                {
                    coll.transform.position = door1.transform.position;
                }
            }
        }
        
    }
    private void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == "Player")
        {
            ani.SetBool("OpenDoor", false);
            
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public int numberOfBomb;
    private int bomWhenStart; // bien luu so bom ban dau, gia tri nay khong thay doi. dung khi reborn player

    [Header("Camera")]
    public Animator cameraShake;
    public GameObject cameraParentObject;

    [Header("Health Of Player")]
    public int healthOfPlayer;

    [Header("End Game")]
    public List<GameObject> listEnemy;
    bool win = false;

    bool defaultMode = true;

    [Header("Mode")] // chi theo o 1 so truogn hop
    public bool mode1;// chien thang duoc check theo so luong enemy bi tru dan di
    public int numOfEnemy;
    private int countNumOfEnemyDie = 0;


    void Start()
    {
        bomWhenStart = numberOfBomb;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(mode1==true)
        {
            if(numOfEnemy==countNumOfEnemyDie)
            {
                win = true;
                UIManager.singleton.SetWinGameTrue();
            }
        }
        else if (defaultMode == true)
        {
            if (win == false)
            {
                CheckEndGame();
            }
        }
    }

    public void ShakeCamera()
    {
        cameraShake.SetTrigger("Shake");
    }

    public void CheckEndGame()
    {
        //int dem = 0;
        for(int i=0;i<listEnemy.Count;i++)
        {
            if(listEnemy[i].activeSelf==false)
            {
                listEnemy.RemoveAt(i);
            }
        }
        if (listEnemy.Count == 0 && Player.singleton.GetDiePlayer() == false)
        {
            win = true;
            UIManager.singleton.SetWinGameTrue();
        }
    }

    void SetWinGame()
    {
        if(Player.singleton.GetDiePlayer() == false)
        {
            
        }
    }

    public int GetNumberOfEnemy()
    {
        if(mode1==false && defaultMode==true)
        {
            return listEnemy.Count;
        }
        return numOfEnemy-countNumOfEnemyDie;
    }

    public int GetNumberOfBomb()
    {
        return numberOfBomb;
    }

    public void SubtractBom(int i)
    {
        if(numberOfBomb>0)
        {
            numberOfBomb -= i;
        }
        if(numberOfBomb==0)
        {
            Invoke("End", 6f);
        }
        
    }

    void End()// goi khi het bom
    {
        if(win==false)
        {
            UIManager.singleton.SetLoseGameTrue();
        }
    }

    public void UpNumberOfEnemyDie()
    {
        if(mode1==true)
        {
            countNumOfEnemyDie++;
        }
    }

    public void MoveCamera(Vector2 pos)
    {
        Vector2 smoothPos = Vector2.Lerp(cameraParentObject.transform.position, pos,0.1f*Time.deltaTime);
        cameraParentObject.transform.position = smoothPos;
    }

    public void CameraFollowPlayer(Vector2 obj)
    {
        Vector2 smoothPos = Vector2.Lerp(cameraParentObject.transform.position, obj, 3f * Time.deltaTime);
        cameraParentObject.transform.position = smoothPos;
    }

    // co the gop ham nay voi ham CameraFollowPlayer(Vector2 obj)
    public void CameraFollowPlayer_X_Or_Y(float x, float y)
    {
        Vector2 smoothPos = Vector2.Lerp(cameraParentObject.transform.position, new Vector2(x, y), 5 * Time.deltaTime);
        cameraParentObject.transform.position = smoothPos;
    }

    public void ResetNumberOfBom()// reborn player nên reset laij soos bom
    {
        numberOfBomb = bomWhenStart;
    }
}

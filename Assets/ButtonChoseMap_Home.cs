﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class ButtonChoseMap_Home : MonoBehaviour
{
    public TextMeshProUGUI text;
    string s;
    private void Start()
    {
        s = this.gameObject.name;
        text.text = s;
    }

    public void ButtonChoseMap()
    {
        SceneManager.LoadScene("Level"+s);
    }
}

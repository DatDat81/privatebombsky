﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Singleton<Player>
{
    [Header("Move")]
    public Joystick joystick;
    public Rigidbody2D rb;
    public Animator ani;
    public float speed;
    private bool faceRight=true;
    public bool isMovingLeft;
    public bool isMovingRight;

    [Header("Jump")]
    public float jumpHight;
    public float distance;
    public LayerMask whatIsGround;
    public Transform point;
    public bool isGround;
    public float fallGravity = 0.25f;

    [Header("Bomb")]
    public GameObject bombBar;
    public Transform pointBomb;
    GameObject bombInstance;
    float forcex, forcey;
    public float maxForce;
    bool isBombButtonDown = false;

    [Header("Die")]
    bool die = false;
    public CapsuleCollider2D colliderWhenAlive;
    public CapsuleCollider2D colliderWhenDie;

    [SerializeField]
    [Header("Health")]
    private int healthOfPlayer;
    private float timeHit=1f;
    private float counter;
    bool alreadyToHit=true; // sau khi bij Hit thif phải sau 1 khoảng thời gian player mới chịu lại đòn tiếp theo

    // luu y:  chi chon 1 trong 3 mode follow
    [Header("Camera Follow Mode")]
    public bool cameraFollowMode=false;

    [Header("Camera Follow X")]
    public bool modeFollow_X=false;
    public float y_position;

    [Header("Camera Follow Y")]
    public bool modeFollow_Y=false;
    public float x_position;
    /// <summary>
    /// 
    /// </summary>

    // save position
    float save_X=float.MaxValue;
    float save_Y = float.MaxValue;
    
    void Start()
    {
        counter = timeHit;
        //lay so health tu game manager
        healthOfPlayer = GameManager.singleton.healthOfPlayer;
        UIManager.singleton.SetHealth(healthOfPlayer);

        colliderWhenAlive.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        isGround = Physics2D.Raycast(point.position, Vector2.down, distance, whatIsGround);
        rb.velocity = new Vector2(joystick.Horizontal * speed, rb.velocity.y);
        // move by joyStick
        if (joystick.Horizontal>0.1f)
        {
            faceRight = true;
            MoveRightButton();
            if (isGround)
            {
                ani.SetBool("Move", true);
            }
            Flip(true);
        }
        else if(joystick.Horizontal < -0.1f)
        {
            faceRight = false;
            isMovingRight = false;
            MoveLeftButton();
            if (isGround)
            {
                ani.SetBool("Move", true);
            }
            Flip(false);
        }
        else
        {
            ani.SetBool("Move", false);
        }

        
        //if (isMovingLeft && die==false)
        //{
        //    faceRight = false;
        //    MoveLeftButton();
        //    if(isGround)
        //    {
        //        ani.SetBool("Move", true);
        //    }
        //    Flip();
        //}
        //else if(isMovingRight && die == false)
        //{
        //    faceRight = true;
        //    MoveRightButton();
        //    if (isGround)
        //    {
        //        ani.SetBool("Move", true);
        //    }
        //    Flip();
        //}



        //fall
        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallGravity-1)*Time.deltaTime;
        }

        // count bomb force
        if (isBombButtonDown == true)
        {
            if (forcex <= maxForce && forcey <= maxForce)
            {
                forcex += 0.1f;
                forcey += 0.1f;
                
            }

        }

        if(cameraFollowMode==true)
        {
            if(isGround==true)
            {
                GameManager.singleton.CameraFollowPlayer(new Vector2(pointBomb.position.x, pointBomb.position.y));
            }
        }
        else if(modeFollow_X==true && modeFollow_Y==false)
        {
            GameManager.singleton.CameraFollowPlayer_X_Or_Y(transform.position.x, y_position);
        }
        else if(modeFollow_X == false && modeFollow_Y == true)
        {
            GameManager.singleton.CameraFollowPlayer_X_Or_Y(x_position, transform.position.y);
        }


        // count time hit
        if(counter>0)
        {
            counter -= Time.deltaTime;
        }

    }


    //controller
    public void MoveLeftButton()
    {
        rb.velocity = new Vector2(-speed, rb.velocity.y);
    }

    public void MoveRightButton()
    {
        rb.velocity = new Vector2(speed,rb.velocity.y) ; // dung velocity se dan den tinh trang di tren khong trung
        //rb.AddForce(Vector2.right * speed);
    }

    public void StopMove()
    {
        rb.velocity = Vector2.zero;
        ani.SetBool("Move", false);
    }

    void Flip(bool face_Right)
    {
        Vector2 local = this.transform.localScale;
        if(face_Right==false)
        {
            local.x = -1;
            this.transform.localScale = local;
        }
        else
        {
            local.x = 1;
            this.transform.localScale = local;
        }
    }
    
    public void Jump()
    {
        
        if(isGround && die == false)
        {
            rb.velocity = Vector2.up * jumpHight ;
            ani.SetTrigger("Jump");
        }
    }

    //Bomb button
    
    public void BombButtonDown()
    {
        if(die==false)
        {
            bombBar.SetActive(true);
            forcex = forcey = 0;
            isBombButtonDown = true;
        }
    }

    public void BombButtonUp()
    {
        isBombButtonDown = false;
        if(die==false)
        {
            bombInstance = ObjectPool.singleton.GetBomb();
            bombInstance.transform.position = pointBomb.position;
            bombInstance.SetActive(true);
            if (faceRight == true)
            {
                bombInstance.GetComponent<Rigidbody2D>().velocity = new Vector2(forcex, forcey);
            }
            else
            {
                bombInstance.GetComponent<Rigidbody2D>().velocity = new Vector2(-forcex, forcey);
            }

            bombInstance.GetComponent<Bomb>().startCount = true; // bat dau dem thoi gian no
            bombInstance.GetComponent<Bomb>().bombOn = true; // animation

            bombBar.SetActive(false);
        }
    }

    // Player Die
    public void SetDiePlayer()
    {
        die = true;
        ani.SetBool("Die", die);
        colliderWhenAlive.enabled = false;
        colliderWhenDie.enabled = true;

        joystick.Horizontal = 0;

        ObjectPool.singleton.SetBomActiverFalse();
        SoundManager.singleton.PlayerDieSound();
        //set lose game
        
        UIManager.singleton.SetLoseGameTrue();
    }

    // Hit Player
    public void HitPlayer()
    {
        if(counter<=0)
        {
            ani.SetTrigger("Hit");
            healthOfPlayer -= 1;
            SoundManager.singleton.PlayerHitSound();
            UIManager.singleton.SetHealth(healthOfPlayer);
            if (healthOfPlayer == 0)
            {
                SetDiePlayer();
            }
            counter = timeHit;
        }
    }

    // Add Health
    public void AddHealth()
    {
        healthOfPlayer += 1;
        UIManager.singleton.SetHealth(healthOfPlayer);
    }

    public bool GetDiePlayer()
    {
        return die;
    }

    public Vector2 GetPositionOfPlayer()
    {
        return this.transform.position;
    }

    public void SetPosition(Vector2 pos)
    {
        transform.position = pos;
    }

    public void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.collider.tag=="OtherEnemy")
        {
            HitPlayer();
        }
        else if(coll.collider.tag=="Heart")
        {
            AddHealth();
            SoundManager.singleton.HeartItemSound();
            coll.collider.gameObject.SetActive(false);
        }
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "SavePosition")
        {
            save_X = coll.transform.position.x;
            save_Y = coll.transform.position.y;
        }
        
    }

    public void SetAnimationDoorOut()
    {
        ani.SetTrigger("DoorOut");
    }

    // set mode cam
    public void CameraFollowMode()
    {
        cameraFollowMode = true;
        modeFollow_X = false;
        modeFollow_Y = false;
    }
    public void CameraFollow_X_Mode(float y)
    {
        cameraFollowMode = false;
        modeFollow_X = true;
        modeFollow_Y = false;
        y_position = y;
    }
    public void CameraFollow_Y_Mode(float x)
    {
        cameraFollowMode = false;
        modeFollow_X = false;
        modeFollow_Y = true;
        x_position = x;
    }

    public void Reborn()
    {
        die = false;
        AddHealth();
        ani.SetBool("Reborn", true);       
        ani.SetBool("Die", false);

        colliderWhenAlive.enabled = true;
        colliderWhenDie.enabled = false;

        joystick.Horizontal = 0;

        GameManager.singleton.ResetNumberOfBom();
        if(save_X!=float.MaxValue || save_Y!=float.MaxValue)
        {
            transform.position = new Vector2(save_X, save_Y);
        }

        bombBar.SetActive(false);
        forcex = 0;
        forcey = 0;

        Invoke("ResetReborn", 0.5f);
    }

    void ResetReborn()
    {      
        ani.SetBool("Reborn", false); // de khong bị vong lap
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    // Start is called before the first frame update
    void OnEnable()
    {
        
        Invoke("SetActive", 3f);
    }

    void SetActive()
    {
        this.gameObject.SetActive(false);
    }
}

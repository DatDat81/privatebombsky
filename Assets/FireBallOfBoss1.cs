﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBallOfBoss1 : MonoBehaviour
{
    public Rigidbody2D rb;
    public Transform pos1;
    public Transform pos2;
    public float Speed;

    private void Update()
    {
        rb.velocity = new Vector2(-pos1.position.x + pos2.position.x, -pos1.position.y + pos2.position.y) * Speed;
    }

}

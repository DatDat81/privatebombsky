﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToOtherScene : MonoBehaviour
{
    public Transform startPos;
    public Transform endPos;
    public Transform destinationCamPos;
    bool move = false;
    bool set = false;
    private void Start()
    {
        transform.position = startPos.position;
    }

    private void Update()
    {
        if(move==true && Vector2.Distance(transform.position,endPos.position)>=0.2f)
        {
            transform.position = Vector2.MoveTowards(transform.position, endPos.position, Time.deltaTime);
            GameManager.singleton.MoveCamera(destinationCamPos.position);
            
        }
        else if(move == true && Vector2.Distance(transform.position, endPos.position) < 0.2f && set==false)
        {
            Player.singleton.SetPosition(new Vector2(endPos.position.x, endPos.position.y + 1f));
            set = true;
        }
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.collider.tag=="Player")
        {
            move = true;
        }
    }
}

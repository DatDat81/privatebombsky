﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpPicture : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("SetActiver", 10f);
    }
    
    void SetActiver()
    {
        gameObject.SetActive(false);
    }
}

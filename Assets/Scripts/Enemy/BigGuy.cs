﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigGuy : MonoBehaviour
{
    [Header("Property")]
    public Rigidbody2D rb;
    public float runSpeed;
    public bool faceRight = true;
    private bool die=false;
    public CapsuleCollider2D collWhenAlive;
    public CapsuleCollider2D collWhenDie;

    [Header("Find Player & Bomb")]
    public Transform rayPos; // dung cho ca player & bomb
    public float disToPlayer; // 
    public float disToBomb; // 
    public LayerMask whatIsPlayer;
    public float checkPos; // dung cho ca player & bomb
    public float disToStop; // dung cho ca player & bomb
    public LayerMask whatIsBomb;
    RaycastHit2D collRightPlayer;
    RaycastHit2D collLeftPlayer;
    RaycastHit2D collBombRight;
    RaycastHit2D collBombLeft;

    [Header("Attack Player")]
    public Transform attackPos;


    [Header("Pick Bom")]
    public Transform findBomPos; // do diem cam bom va khoang cach bom xa nen can dung findbomPos vowis radius de tim bom duoi chan
    public Transform pickBombPos;
    public float radius;
    public float forceBom;
    bool pickBom = false;

    [Header("ThrowBomb")]
    bool throwBomb = false;
    Collider2D collBombTrow;

    [Header("Animation")]
    public Animator ani;

    void Start()
    {
        
    }

    void Update()
    {
        FaceRight(faceRight);
        FindBomb();
        if (collBombLeft.collider == null && collBombRight.collider == null)
        {
            FindPlayer();
        }



        if (pickBom == true && throwBomb == false)
        {
            collBombTrow = Physics2D.OverlapCircle(findBomPos.position, radius, whatIsBomb);
            if (collBombTrow != null && collBombTrow.GetComponent<Bomb>().bombOn == true)
            {

                collBombTrow.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                ani.SetBool("PickBomb", pickBom);

                if (Vector2.Distance(collBombTrow.transform.position, pickBombPos.position) >= 0.2f && throwBomb == false)
                {
                    collBombTrow.transform.position = Vector2.MoveTowards(collBombTrow.transform.position, pickBombPos.position, 5 * Time.deltaTime);
                    if (Vector2.Distance(collBombTrow.transform.position, pickBombPos.position) <= 0.2f)
                    {
                        throwBomb = true;
                        if (Player.singleton.GetPositionOfPlayer().x >= transform.position.x)
                        {
                            faceRight = true;
                            FaceRight(true);
                        }
                        else
                        {
                            faceRight = false;
                            FaceRight(false);
                        }
                    }
                }

                if (throwBomb == true)
                {
                    Invoke("ThrowBomb", 0.2f);

                }
            }
            else
            {
                throwBomb = false;
                pickBom = false;
                ani.SetBool("PickBomb", false);
            }
        }
    }

    void ThrowBomb()
    {
        throwBomb = false;
        pickBom = false;
        ani.SetBool("PickBomb", false);
        Vector2 direction;
        direction.x = Player.singleton.GetPositionOfPlayer().x - transform.position.x;
        direction.y = Player.singleton.GetPositionOfPlayer().y - transform.position.y;
        direction.Normalize();
        GameManager.singleton.ShakeCamera();
        collBombTrow.GetComponent<Rigidbody2D>().velocity = direction * forceBom;
        collBombTrow.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
    }

    void FindPlayer()
    {
        collRightPlayer = Physics2D.Raycast(rayPos.position, Vector2.right, disToPlayer, whatIsPlayer);
        collLeftPlayer = Physics2D.Raycast(rayPos.position, Vector2.left, disToPlayer, whatIsPlayer);

        if (collRightPlayer.collider!=null && die==false && Player.singleton.GetDiePlayer()==false)
        {
            bool isDestination= false; // check xem da den dien de tan cong player chua
            bool attacked = false;
            float des = Vector2.Distance(transform.position, collRightPlayer.collider.transform.position);
            if(des<= disToStop)
            {
                faceRight = true;
                isDestination = true;
                ani.SetBool("Run", false);
                if(attacked==false)
                {
                    attacked = true;
                    ani.SetBool("Attack", attacked);
                    Invoke("AttackPlayer", 0.2f);
                }
            }
            else
            {
                attacked = false;
                ani.SetBool("Attack", attacked);
                isDestination = false;
            }

            if(isDestination==false)
            {
                faceRight = true;
                ani.SetBool("Run", true);
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(collRightPlayer.collider.transform.position.x-checkPos, transform.position.y), runSpeed * Time.deltaTime);
            }
        }
        else if(collLeftPlayer.collider != null && die == false && Player.singleton.GetDiePlayer() == false)
        {
            bool isDestination = false; // check xem da den dien de tan cong player chua
            bool attacked = false;
            float des = Vector2.Distance(transform.position, collLeftPlayer.collider.transform.position);
            if (des <= disToStop)
            {
                faceRight = false;
                isDestination = true;
                ani.SetBool("Run", false);
                if (attacked == false)
                {
                    attacked = true;
                    ani.SetBool("Attack", attacked);
                    Invoke("AttackPlayer", 0.2f);
                }
            }
            else
            {
                attacked = false;
                ani.SetBool("Attack", attacked);
                isDestination = false;
            }

            if (isDestination == false)
            {
                faceRight = false;
                ani.SetBool("Run", true);
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(collLeftPlayer.collider.transform.position.x+checkPos, transform.position.y), runSpeed * Time.deltaTime);
            }
        }
        else
        {

            ani.SetBool("Run", false);
        }

    }

    void AttackPlayer()
    {
        Collider2D collPlayer = Physics2D.OverlapCircle(attackPos.position, 0.5f, whatIsPlayer);
        if (collPlayer != null && die == false)
        {
            if (faceRight)
            {
                GameManager.singleton.ShakeCamera();
                collPlayer.GetComponent<Rigidbody2D>().velocity = new Vector2(1, 1) * 5f;
                //collPlayer.GetComponent<Player>().SetDiePlayer();
                collPlayer.GetComponent<Player>().HitPlayer();
            }
            else
            {
                GameManager.singleton.ShakeCamera();
                //collPlayer.GetComponent<Player>().SetDiePlayer();
                collPlayer.GetComponent<Player>().HitPlayer();
                collPlayer.GetComponent<Rigidbody2D>().velocity = new Vector2(-1, 1) * 5f;
            }
        }
    }


    /// <summary>
    /// ////////////////////////////////////////////// BOM ////////////////////////////////////////////////////////////////////////////////////
    /// </summary>
    
    void FindBomb()
    {
        collBombRight = Physics2D.Raycast(rayPos.position, Vector2.right, disToBomb, whatIsBomb);
        collBombLeft = Physics2D.Raycast(rayPos.position, Vector2.left, disToBomb, whatIsBomb);

        if (collBombRight.collider != null && die == false && Player.singleton.GetDiePlayer() == false && collBombRight.collider.GetComponent<Bomb>().bombOn==true)
        {
            bool isDestination = false; // check xem da den dien de tan cong player chua
            //bool pickBom = false;
            float des = Vector2.Distance(transform.position, collBombRight.collider.transform.position);
            if (des <= 0.3f)
            {
                isDestination = true;
                ani.SetBool("Run", false);
                if(pickBom==false)
                {
                    pickBom = true;

                }
            }
            else
            {
                isDestination = false;
            }

            if (isDestination == false && pickBom==false)
            {
                faceRight = true;
                ani.SetBool("Run", true);
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(collBombRight.collider.GetComponent<Bomb>().pointCenter.position.x, transform.position.y), (runSpeed+0.5f) * Time.deltaTime);
            }
        }
        else if (collBombLeft.collider != null && die == false && Player.singleton.GetDiePlayer() == false && collBombLeft.collider.GetComponent<Bomb>().bombOn == true)
        {
            bool isDestination = false; // check xem da den dien de tan cong player chua
            
            float des = Vector2.Distance(transform.position, collBombLeft.collider.transform.position);
            if (des <= 0.3f)
            {
                isDestination = true;
                ani.SetBool("Run", false);
                if (pickBom == false)
                {
                    pickBom = true;
                }
            }
            else
            {
                isDestination = false;
            }

            if (isDestination == false && pickBom==false)
            {
                faceRight = false;
                ani.SetBool("Run", true);
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(collBombLeft.collider.GetComponent<Bomb>().pointCenter.position.x, transform.position.y), (runSpeed + 0.5f) * Time.deltaTime);
            }
        }
    }


    /// <summary>
    /// /////////////////////////////////////////////////////////////////Property///////////////////////////////////////////////////
    /// </summary>
    /// <param name="right"></param>
    void FaceRight(bool right)
    {
        Vector2 local = this.transform.localScale;
        if (right == true)
        {
            local.x = 1;
        }
        else
        {
            local.x = -1;
        }
        this.transform.localScale = local;
    }
    bool upEnemyDie = false;
    public void SetDieEnemy()
    {
        die = true;
        ani.SetBool("Die", die);
        collWhenAlive.enabled = false;
        collWhenDie.enabled = true;
        if (upEnemyDie == false) // chi tang so luong enemy die 1 lan
        {
            GameManager.singleton.UpNumberOfEnemyDie();
            upEnemyDie = true;
        }
        Invoke("SetActiverFalse", 2.5f);
    }

    void SetActiverFalse()
    {
        gameObject.SetActive(false);
    }
}

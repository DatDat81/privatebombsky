﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaldPirate : MonoBehaviour
{


    [Header("Mode")]
    public bool staticMode;
    public bool notFollowPlayer;
    [Header("Property")]
    public bool faceRight = true;
    public float speed;
    public Rigidbody2D rb;
    public bool die=false; // check enymy da chet chua
    public CapsuleCollider2D colliderWhenAlive;
    public CapsuleCollider2D colliderWhenDie;

    [Header("Find Player and Bomb")]
    public LayerMask whatIsPlayer; 
    public Transform rayPos;
    public float distanceToPlayer;
    public float distanceToBomb;
    private RaycastHit2D isPlayerRight;
    private RaycastHit2D isPlayerLeft;
    public float checkPos;
    public float distandeToStop;
    //bomb
    public LayerMask whatIsBomb;
    RaycastHit2D collRight;
    RaycastHit2D collLeft;

    public LayerMask whatIsTarget;

    [Header("Check Ground")]
    public Transform groundPos;
    private float distanceToGround=0.2f;
    public LayerMask whatIsGround;
    public bool isGround = false;

    [Header("Animation")]
    public Animator ani;
    bool run=false;
    bool attacked = false;

    [Header("Attack")]
    public Transform posSetAttack;
    bool canAttackPl = false;

    void Start()
    {
        
    }


    void Update()
    {
        CheckGround(); // kiem tra co dung o duoi dat hay k

        if(staticMode==false)
        {
            FindBomb();      // uu tien tim boom truoc
            if (collRight.collider == null && collLeft.collider == null)
            {
                FindPLayer();
            }
        }

        // sau khi da bomb xong se tim va danh player

        if (notFollowPlayer == false)// neu che do notfollow ==false
        {
            if (canAttackPl == true && Player.singleton.GetDiePlayer() == false && die == false)
            {
                float des;
                des = Mathf.Abs(this.transform.position.x - Player.singleton.GetPositionOfPlayer().x);
                if (des >= 0.5f)
                {
                    // chay den vi tri player
                    run = true;
                    ani.SetBool("Run", run);
                    if (attacked == false && IsInvoking() == false)
                    {
                        if (this.transform.position.x <= Player.singleton.GetPositionOfPlayer().x)
                        {
                            Flip(true);
                        }
                        else
                        {
                            Flip(false);
                        }
                    }

                    this.transform.position = Vector2.MoveTowards(this.transform.position, new Vector2(Player.singleton.GetPositionOfPlayer().x - 0.5f, transform.position.y), speed * Time.deltaTime);
                }
                else
                {
                    canAttackPl = false;
                    run = false;// set anim khong chay nuwa khi den dich
                    ani.SetBool("Run", run);
                    if (CheckPositionPlayerToJump() == true && isGround == true)
                    {
                        rb.velocity = Vector2.up * 8;
                    }
                    if (rb.velocity.y < 0)
                    {
                        rb.velocity += Vector2.up * Physics2D.gravity.y * (1.71f - 1) * Time.deltaTime;
                    }
                }
            }
        }
    }

    void CheckGround()
    {
        isGround = Physics2D.Raycast(groundPos.position, Vector2.down, distanceToGround, whatIsGround);
    }

    private void Flip(bool right=true)
    {
        if (right == true && die == false)
        {
            FaceRight(true);
            faceRight = true;
        }
        else if (right == false && die == false)
        {
            FaceRight(false);
            faceRight = false;
        }
    }    


    /// <summary>
    /// //////////////////////////////////////////////////////// PLAYER ///////////////////////////////////////////////////////////////////////////
    /// </summary>
    
    public void FindPLayer() // Player
    {
        isPlayerRight = Physics2D.Raycast(rayPos.position, Vector2.right, distanceToPlayer, whatIsPlayer);
        isPlayerLeft = Physics2D.Raycast(rayPos.position, Vector2.left, distanceToPlayer, whatIsPlayer);
        

        if (isPlayerRight.collider!=null  && die == false && Player.singleton.GetDiePlayer() == false)//&& isGround == true
        {
            bool isdestination = false;
            float distanceToPlayer = Vector2.Distance(this.transform.position, isPlayerRight.collider.transform.position);
            if (distanceToPlayer <= distandeToStop)
            {
                isdestination = true;
                if (attacked == false && Player.singleton.GetDiePlayer() == false) // check de chay ani attack 1 lan
                {
                    ani.SetTrigger("Attack");
                    attacked = true;
                    Debug.Log("in");
                    //Invoke("Attack", 0.3f);
                }
            }
            else
            {
                isdestination = false;
                attacked = false;
            }

            if (isdestination == false)
            {
                Flip(true);
                run = true;
                ani.SetBool("Run", run);
                this.transform.position = Vector2.MoveTowards(this.transform.position, new Vector2(isPlayerRight.collider.transform.position.x - checkPos, this.transform.position.y), speed * Time.deltaTime);
            }
            else
            {
                run = false;
                ani.SetBool("Run", run);
            }

        }
        else if (isPlayerLeft.collider!=null && die == false && Player.singleton.GetDiePlayer() == false)//&& isGround == true 
        {
            bool isdestination = false;
            float distanceToPlayer = Vector2.Distance(this.transform.position, isPlayerLeft.collider.transform.position);
            if (distanceToPlayer <= distandeToStop)
            {
                isdestination = true;
                if (attacked == false && Player.singleton.GetDiePlayer() == false) // check de chay ani attack 1 lan
                {
                    ani.SetTrigger("Attack");
                    attacked = true;
                    //Invoke("Attack", 0.3f);
                    Debug.Log("in");
                }
            }
            else
            {
                isdestination = false;
                attacked = false;
            }

            if (isdestination == false)
            {
                Flip(false);
                run = true;
                ani.SetBool("Run", run);
                this.transform.position = Vector2.MoveTowards(this.transform.position, new Vector2(isPlayerLeft.collider.transform.position.x + checkPos, this.transform.position.y), speed * Time.deltaTime);
            }
            else
            {
                run = false;
                ani.SetBool("Run", run);
            }
        }
        else
        {
            run = false;
            ani.SetBool("Run", run);
        }
    }

    /// <summary>
    /// ///////////////////////////////////////////////////////////////////////BOM ////////////////////////////////////////////////////////////////////////
    /// </summary>

    public void FindBomb() // tim bom
    {
        collRight = Physics2D.Raycast(rayPos.position, Vector2.right, distanceToBomb, whatIsBomb);// check phai
        collLeft = Physics2D.Raycast(rayPos.position, Vector2.left, distanceToBomb, whatIsBomb); // check coll trai

        
        if (collRight.collider != null && die == false) // khi ma co bom && collRight.collider.GetComponent<Bomb>().bombOn == true
        {
            canAttackPl = false;
            Flip(true);
            bool isdestinationBomb = false; // kiem tra da den vi tri de attack chua
            float distanceToBomb = Vector2.Distance(this.transform.position, collRight.collider.transform.position);
            if (distanceToBomb <= distandeToStop) // den du gan de da vao bom
            {
                //Flip(true);
                isdestinationBomb = true; 
                if (attacked == false) // check de chay ani attack 1 lan
                {
                    ani.SetTrigger("Attack");
                    attacked = true;
                    //Jump();//nhay
                    //Invoke("Attack", 0.3f); // danh muc tieu                   
                }
                run = false;
                ani.SetBool("Run", run);
            }
            else
            {
                isdestinationBomb = false;
                attacked = false;
            }

            if (isdestinationBomb == false) // khi ma chua tiep can muc tieu thi se tiep tuc duoi theo
            {
                Flip(true);
                run = true;
                ani.SetBool("Run", run);
                this.transform.position = Vector2.MoveTowards(this.transform.position, new Vector2(collRight.collider.transform.position.x - checkPos, this.transform.position.y), speed * Time.deltaTime);

            }
            else // duoi duoc muc tieu
            {
                run = false;
                ani.SetBool("Run", run);
            }
        }
        else if(collLeft.collider!=null && die==false)//&& collLeft.collider.GetComponent<Bomb>().bombOn == true
        {
            canAttackPl = false;
            Flip(false);
            bool isdestinationBomb = false; // kiem tra da den vi tri de attack chua
            float distanceToBomb = Vector2.Distance(this.transform.position, collLeft.collider.transform.position);
            // check khoang cach
            if (distanceToBomb <= distandeToStop) // den du gan de da vao bom
            {
                //faceRight = false;
                isdestinationBomb = true;
                if (attacked == false) // check de chay ani attack 1 lan
                {
                    if (transform.position.x > Player.singleton.GetPositionOfPlayer().x)
                    {
                        Flip(false);
                    }
                    else
                    {
                        Flip(true);
                    }
                    ani.SetTrigger("Attack");
                    attacked = true;
                    //Invoke("Attack", 0.3f); // danh muc tieu
                    //Jump();
                }
            }
            else
            {
                isdestinationBomb = false;
                attacked = false;
            }

            // chay den muc tieu
            if (isdestinationBomb == false) // khi ma chua tiep can muc tieu thi se tiep tuc duoi theo
            {
                Flip(false);
                run = true;
                ani.SetBool("Run", run);
                this.transform.position = Vector2.MoveTowards(this.transform.position, new Vector2(collLeft.collider.transform.position.x - checkPos, this.transform.position.y), speed * Time.deltaTime);

            }
            else // duoi duoc muc tieu
            {
                run = false;
                ani.SetBool("Run", run);
            }
        }
        else
        {
            run = false;
            ani.SetBool("Run", run);
        }
    }

    public void FaceRight(bool right)
    {
        Vector2 local = this.transform.localScale;
        if(right==true)
        {
            local.x = 1;
        }
        else
        {
            local.x = -1;
        }
        this.transform.localScale = local;
    }

    

    void Attack()
    {
        Collider2D collPlayer = Physics2D.OverlapCircle(posSetAttack.position, 0.5f, whatIsPlayer);
        Collider2D collBomb = Physics2D.OverlapCircle(posSetAttack.position, 0.5f, whatIsBomb);
        if(collPlayer!=null && die == false)
        {
            if (faceRight)
            {
                
                collPlayer.GetComponent<Rigidbody2D>().velocity = new Vector2(1, 1) * 5f;
                //collPlayer.GetComponent<Player>().SetDiePlayer();
                collPlayer.GetComponent<Player>().HitPlayer();
                GameManager.singleton.ShakeCamera();
            }
            else
            {
                GameManager.singleton.ShakeCamera();               
                collPlayer.GetComponent<Rigidbody2D>().velocity = new Vector2(-1, 1) * 5f;
                //collPlayer.GetComponent<Player>().SetDiePlayer();
                collPlayer.GetComponent<Player>().HitPlayer();
            }
        }
        else if (collBomb != null && die == false)
        {
            if (faceRight)
            {
                GameManager.singleton.ShakeCamera();
                collBomb.GetComponent<Rigidbody2D>().velocity = new Vector2(1, 1) * 5f;
            }
            else
            {
                GameManager.singleton.ShakeCamera();
                collBomb.GetComponent<Rigidbody2D>().velocity = new Vector2(-1, 1) * 5f;
            }
        }
        SoundManager.singleton.BaldPirateAttackSound();
    }
    public void CanAttackPlayer()
    {
        canAttackPl = true;
        // check enemy huong mat ve player
        
    }

    public void ResetAttack()
    {
        attacked = false;
    }    

    bool CheckPositionPlayerToJump() // neu toa do y cua ênmy va player chenh lech nhieu thi se nhay
    {
        float ps = Mathf.Abs(transform.position.y - Player.singleton.GetPositionOfPlayer().y);
        if (ps <= 1f)
            return false;
        return true;
    }
    bool upEnemyDie = false;
    public void SetDieEnemy()
    {
        die = true;
        ani.SetBool("Die", die);
        SoundManager.singleton.BaldPirateDeathSound();
        colliderWhenAlive.enabled = false;
        colliderWhenDie.enabled = true;
        if (upEnemyDie == false) // chi tang so luong enemy die 1 lan
        {
            GameManager.singleton.UpNumberOfEnemyDie();
            upEnemyDie = true;
        }
        Invoke("SetActiverFalse", 2.5f);
    }

    void SetActiverFalse()
    {
        gameObject.SetActive(false);
    }

}

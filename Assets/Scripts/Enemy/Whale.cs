﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Whale : MonoBehaviour
{
    [Header("Property")]
    public Rigidbody2D rb;
    public Animator ani;
    public float runSpeed;
    private bool die = false;
    private bool faceLeft = true;

    [Header("Find Player")]
    public Transform rayPos;
    public float distanceToPlayer;// khoang cach tim thay player
    public float distanceToBomb;// khoang cach tim thay bomb
    public float distanceToStop;
    public float checkPos;
    public LayerMask whatIsPlayer;
    public Transform attackPos;

    [Header("Find Bomb")]
    public LayerMask whatIsBomb;
    RaycastHit2D collBombRight;
    RaycastHit2D collBombLeft;
    bool swalow = false;
    public Transform swalowPos;
    public float speedSwalow;
    Collider2D collSwalowBomb;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        FaceLeft(faceLeft);
        FindBomb();
        if (collBombLeft.collider == null && collBombRight.collider == null)
        {
            FindPlayer();
        }

        if (swalow == true)
        {
            collSwalowBomb = Physics2D.OverlapCircle(swalowPos.position, 0.5f, whatIsBomb);
            if (collSwalowBomb != null)
            {
                ani.SetBool("Swalow", true);
                if (Vector2.Distance(collSwalowBomb.transform.position, swalowPos.position) >= 0.2f)
                {
                    collSwalowBomb.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
                    collSwalowBomb.transform.position = Vector2.MoveTowards(collSwalowBomb.transform.position, swalowPos.position, speedSwalow * Time.deltaTime);
                }
                else
                {
                    Invoke("SwalowBomb", 0.2f);
                }
            }
        }
    }

    void SwalowBomb()
    {
        ani.SetBool("Swalow", false);
        GameManager.singleton.ShakeCamera();
        if(collSwalowBomb!=null)
        {
            SoundManager.singleton.WhaleSwalowBomSound();
            collSwalowBomb.gameObject.SetActive(false);
        }
        swalow = false;
    }

    /// <summary>
    /// /////////////////////////////////////////////////////////////////// Player
    /// </summary>
    void FindPlayer()
    {
        RaycastHit2D collPlayerRight = Physics2D.Raycast(rayPos.position, Vector2.right, distanceToPlayer, whatIsPlayer);
        RaycastHit2D collPlayerLeft = Physics2D.Raycast(rayPos.position, Vector2.left, distanceToPlayer, whatIsPlayer);
        if(collPlayerRight.collider!=null && die==false && Player.singleton.GetDiePlayer()==false)
        {
            bool isDestination = false;
            bool attacked = false;
            float dis = Mathf.Abs(transform.position.x - collPlayerRight.collider.transform.position.x);
            if(dis<=distanceToStop)
            {
                faceLeft = false; // de phong truong hop nguoi choi nhay tu tren xuong whale, bo qua giai doan chay den playerva danh, thi whale quay mat va attack luon
                ani.SetBool("Run", false);
                isDestination = true;
                if(attacked==false)
                {
                    attacked = true;
                    ani.SetBool("Attack", attacked);
                    Invoke("AttackPlayer", 0.2f);
                }
            }
            else
            {
                isDestination = false;
                attacked = false;
                ani.SetBool("Attack", attacked);
            }
            if(isDestination==false)// khi chua den dim dung
            {
                faceLeft = false;
                ani.SetBool("Run", true);
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(collPlayerRight.collider.transform.position.x - checkPos, transform.position.y), runSpeed * Time.deltaTime);
            }
        }
        else if(collPlayerLeft.collider != null && die == false && Player.singleton.GetDiePlayer() == false)
        {
            bool isDestination = false;
            bool attacked = false;
            float dis = Mathf.Abs(transform.position.x - collPlayerLeft.collider.transform.position.x);
            if (dis <= distanceToStop)
            {
                faceLeft = true;
                ani.SetBool("Run", false);
                isDestination = true;
                if (attacked == false)
                {
                    attacked = true;
                    ani.SetBool("Attack", attacked);
                    Invoke("AttackPlayer", 0.2f);
                }   
            }
            else
            {
                isDestination = false;
                attacked = false;
                ani.SetBool("Attack", attacked);
            }
            if (isDestination == false)// khi chua den dim dung
            {
                faceLeft = true;
                ani.SetBool("Run", true);
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(collPlayerLeft.collider.transform.position.x + checkPos, transform.position.y), runSpeed * Time.deltaTime);
            }
        }
        else
        {
            ani.SetBool("Run", false);
        }
    }
    public void FaceLeft(bool left)
    {
        Vector2 local = this.transform.localScale;
        if (left == true)
        {
            local.x = 1;
        }
        else
        {
            local.x = -1;
        }
        this.transform.localScale = local;
    }

    void AttackPlayer()
    {
        Collider2D collPlayer = Physics2D.OverlapCircle(attackPos.position, 0.5f, whatIsPlayer);
        if (collPlayer != null && die == false)
        {
            if (faceLeft==false)
            {
                GameManager.singleton.ShakeCamera();
                collPlayer.GetComponent<Rigidbody2D>().velocity = new Vector2(1, 1) * 5f;
                collPlayer.GetComponent<Player>().HitPlayer();
            }
            else
            {
                GameManager.singleton.ShakeCamera();
                collPlayer.GetComponent<Player>().HitPlayer();
                collPlayer.GetComponent<Rigidbody2D>().velocity = new Vector2(-1, 1) * 5f;
            }
        }
    }

    /// <summary>
    /// /////////////////////////////////////////////////////////////////////BOMB
    /// </summary>

    void FindBomb()
    {
        collBombRight = Physics2D.Raycast(rayPos.position, Vector2.right, distanceToBomb, whatIsBomb);
        collBombLeft = Physics2D.Raycast(rayPos.position, Vector2.left, distanceToBomb, whatIsBomb);

        if(collBombRight.collider!=null && die==false && Player.singleton.GetDiePlayer()==false)
        {
            bool isDestination = false;
            float dis = Mathf.Abs(transform.position.x - collBombRight.collider.transform.position.x);
            if (dis <= distanceToStop)
            {
                ani.SetBool("Run", false);
                isDestination = true;
                swalow = true;
            }
            else
            {
                isDestination = false;
            }
            if (isDestination == false)// khi chua den dim dung
            {
                faceLeft = false;
                ani.SetBool("Run", true);
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(collBombRight.collider.transform.position.x - checkPos, transform.position.y), runSpeed * Time.deltaTime);
            }
        }else if (collBombLeft.collider != null && die == false && Player.singleton.GetDiePlayer() == false)
        {
            bool isDestination = false;
            float dis = Mathf.Abs(transform.position.x - collBombLeft.collider.transform.position.x);
            if (dis <= distanceToStop)
            {
                ani.SetBool("Run", false);
                isDestination = true;
                swalow = true;
            }
            else
            {
                isDestination = false;
            }
            if (isDestination == false)// khi chua den dim dung
            {
                faceLeft = true;
                ani.SetBool("Run", true);
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(collBombLeft.collider.transform.position.x - checkPos, transform.position.y), runSpeed * Time.deltaTime);
            }
        }
    }
    bool upEnemyDie = false;
    public void SetDieEnemy()
    {
        die = true;   
        ani.SetBool("Die", die);
        if (upEnemyDie == false) // chi tang so luong enemy die 1 lan
        {
            GameManager.singleton.UpNumberOfEnemyDie();
            upEnemyDie = true;
        }
        SoundManager.singleton.WhaleDieSound();
        Invoke("SetActiverFalse", 2.5f);
    }

    void SetActiverFalse()
    {
        gameObject.SetActive(false);
    }
}

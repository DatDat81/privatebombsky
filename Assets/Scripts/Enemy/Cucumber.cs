﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cucumber : MonoBehaviour
{
    [Header("Property")]
    public Rigidbody2D rb;
    public float runSpeed;
    public bool faceLeft = true;
    private bool die=false; // chet
    public CapsuleCollider2D collWhenAlive;
    public CapsuleCollider2D collWhenDie;

    [Header("Ground")]
    public bool isGround;
    public float disToGround;
    public Transform groundPos;
    public LayerMask whatIsGround;

    [Header("Find Player")]
    public float distanceToPlayer;
    public float distanceToBomb;
    private bool findedPlayer;
    public float checkPos;
    public float disToStop;
    public LayerMask whatIsPlayer;
    public Transform rayPos;
    public Transform pointAddFord; // diem de khi danh player se truyen luc theo vec to co diem nay la diem goc 
    public Transform pointCollider;
    RaycastHit2D rayRight;
    RaycastHit2D rayLeft;
    RaycastHit2D rayBombRight;
    RaycastHit2D rayBombLeft;

    [Header("Bomb")]
    public LayerMask whatIsBomb;

    [Header("Animator")]
    public Animator ani;
    private bool run = false;

    [Header("Jump")]
    bool followPlayer = false;
    public float disUntilJump; // khoang cach ve truc y de player nhay
    int dem=0;// dem so lan nhay. toi da 2 lan trong 1 dot tan cong

    void Start()
    {
        
    }


    void Update()
    {
        isGround = Physics2D.Raycast(groundPos.position, Vector2.down, disToGround, whatIsGround);
        if (die == false)
        {
            FindBomb();
            if (rayBombLeft.collider == null && rayBombRight.collider == null) // neu k co player thi moi tim bom, uu tien tim player truoc
            {
                FindPlayer();
            }
        }


        if (followPlayer == true && Player.singleton.GetDiePlayer() == false && die == false)
        {
            if (Mathf.Abs(transform.position.x - Player.singleton.GetPositionOfPlayer().x) > 0) // khi chua du tiep can player ve truc x (toa do)
            {
                if (transform.position.x <= Player.singleton.GetPositionOfPlayer().x)
                {
                    FaceLeft(false);// quay phai
                    run = true;
                    ani.SetBool("Run", run);
                    rb.transform.position = Vector2.MoveTowards(transform.position, new Vector2(Player.singleton.GetPositionOfPlayer().x, transform.position.y), runSpeed * Time.deltaTime);

                }
                else
                {
                    run = true;
                    ani.SetBool("Run", run);
                    FaceLeft(true);
                    rb.transform.position = Vector2.MoveTowards(transform.position, new Vector2(Player.singleton.GetPositionOfPlayer().x, transform.position.y), runSpeed * Time.deltaTime);
                }

            }
            else// khi da du khoang cach thi dung lai va nhay len
            {
                run = false;
                ani.SetBool("Run", run);
                if (Mathf.Abs(transform.position.y - Player.singleton.GetPositionOfPlayer().y) > disUntilJump) // khi chua du tiep can player ve truc x (toa do)
                {
                    if (isGround == true)
                    {
                        if (dem == 0)
                        {
                            Jump();
                            if (Vector2.Distance(transform.position, Player.singleton.GetPositionOfPlayer()) > 1f)
                            {
                                followPlayer = false;
                            }
                        }
                        if (dem == 1)
                        {
                            if (IsInvoking() == false)
                            {
                                Invoke("Jump", 1.9f);
                            }
                        }
                    }
                    if (rb.velocity.y < 0)
                    {
                        rb.velocity += Vector2.up * Physics2D.gravity.y * (1.71f - 1) * Time.deltaTime;
                    }

                }
            }
        }
    }

    void Jump()// viet ham de con Invoke
    {
        rb.velocity = Vector2.up * 8;
        dem++;
    }

    bool attack = false; // kiem tra enemy da attack chuwa, 
    public void FindPlayer()
    {
        rayRight = Physics2D.Raycast(rayPos.transform.position, Vector2.right, distanceToPlayer, whatIsPlayer);
        rayLeft = Physics2D.Raycast(rayPos.transform.position, Vector2.left, distanceToPlayer, whatIsPlayer);

        

        if(rayRight==true && Player.singleton.GetDiePlayer()==false)
        {
            float dis = Vector2.Distance(transform.position, rayRight.collider.transform.position); // khoang cach den player
            bool stopMove;

            if (dis <= disToStop) // neu nam trong khoang cach nay thi dung di chuyen
            {
                FaceLeft(false);
                stopMove = true;
                run = false;
                ani.SetBool("Run", run);
                if (attack == false) // chi attack 1 lan trong pham vi dis<= disToStop
                {
                    Invoke("AttackPlayer", 0.25f);
                    ani.SetTrigger("Attack");
                    attack = true;
                }
            }
            else
            {
                stopMove = false;
                attack = false;// reset
            }

            if (stopMove == false)
            {
                run = true;
                ani.SetBool("Run", run);
                rb.transform.position = Vector2.MoveTowards(transform.position, new Vector2(Player.singleton.GetPositionOfPlayer().x - checkPos, transform.position.y), runSpeed * Time.deltaTime);// chi di chuyen ve toa do x, toa do y giu nguyen
                FaceLeft(false);
            }
            else
            {
                run = false;
                ani.SetBool("Run", run);
            }
        }
        else if (rayLeft == true && Player.singleton.GetDiePlayer() == false)
        {
            float dis = Vector2.Distance(transform.position, rayLeft.collider.transform.position); // khoang cach den player
            bool stopMove;

            if (dis <= disToStop) // neu nam trong khoang cach nay thi dung di chuyen
            {
                FaceLeft(true);
                stopMove = true;
                run = false;
                ani.SetBool("Run", run);
                if (attack == false) // chi attack 1 lan trong pham vi dis<= disToStop
                {
                    Invoke("AttackPlayer", 0.25f);
                    ani.SetTrigger("Attack");
                    attack = true;
                }
            }
            else
            {
                stopMove = false;
                attack = false;// reset
            }

            if (stopMove == false)
            {
                run = true;
                ani.SetBool("Run", run);
                rb.transform.position = Vector2.MoveTowards(transform.position, new Vector2(Player.singleton.GetPositionOfPlayer().x + checkPos, transform.position.y), runSpeed * Time.deltaTime);// chi di chuyen ve toa do x, toa do y giu nguyen
                FaceLeft(true);
            }
            else
            {
                run = false;
                ani.SetBool("Run", run);
            }
        }
        else
        {
            run = false;
            ani.SetBool("Run", run);
        }

    }

    public void FaceLeft(bool left)
    {
        Vector2 local = this.transform.localScale;
        if (left == true)
        {
            local.x = 1;
        }
        else
        {
            local.x = -1;
        }
        this.transform.localScale = local;
    }

    void AttackPlayer()
    {
        Collider2D coll = Physics2D.OverlapCircle(pointCollider.position, 0.5f, whatIsPlayer);
        if(coll!=null)
        {
            GameManager.singleton.ShakeCamera();
            coll.GetComponent<Rigidbody2D>().velocity = new Vector2(coll.transform.position.x- pointAddFord.position.x, coll.transform.position.y- pointAddFord.position.y) * 5f;
            //coll.GetComponent<Player>().SetDiePlayer();
            coll.GetComponent<Player>().HitPlayer();
        }
    }

    ////////////////////////////////////////////////////////////////// bom/////////////////////////////////////////////////////////////////////////////
    bool blow = false; // kiem tra enemy da attack chuwa, 
    public void FindBomb()
    {
        rayBombRight = Physics2D.Raycast(rayPos.transform.position, Vector2.right, distanceToBomb, whatIsBomb); // dung distanceToPlayer cho Bomb luon
        rayBombLeft = Physics2D.Raycast(rayPos.transform.position, Vector2.left, distanceToBomb, whatIsBomb);

        float dis;
        // khoang cach den player
        if (rayBombRight.collider != null && rayBombRight.collider.GetComponent<Bomb>().bombOn == true) // tinh khoang cach den qua bom chua bi OFF
        {
            dis= Vector2.Distance(transform.position, rayBombRight.collider.transform.position);
            bool stopMove=false;
            if (dis <= disToStop) // neu nam trong khoang cach nay thi dung di chuyen
            {
                stopMove = true;
                run = false;
                ani.SetBool("Run", run);
                if (blow == false) // chi attack 1 lan trong pham vi dis<= disToStop
                {
                    
                    ani.SetTrigger("Blow");
                    blow = true;
                    Invoke("BlowTheBomb", 0.25f);
                }
                
            }
            else
            {
                stopMove = false;
                blow = false;// reset
            }

            if (rayBombRight == true && rayBombRight.collider.GetComponent<Bomb>().bombOn == true)
            {
                if (stopMove == false)
                {
                    run = true;
                    ani.SetBool("Run", run);
                    rb.transform.position = Vector2.MoveTowards(transform.position, new Vector2(rayBombRight.collider.transform.position.x - checkPos, transform.position.y), runSpeed * Time.deltaTime);// chi di chuyen ve toa do x, toa do y giu nguyen
                    FaceLeft(false);
                }
                else
                {
                    run = false;
                    ani.SetBool("Run", run);
                }
            }
        }
        else if (rayBombLeft.collider != null && rayBombLeft.collider.GetComponent<Bomb>().bombOn == true)
        {
            dis = Vector2.Distance(transform.position, rayBombLeft.collider.transform.position);
            bool stopMove=false;
            if (dis <= disToStop) // neu nam trong khoang cach nay thi dung di chuyen
            {
                stopMove = true;
                run = false;
                ani.SetBool("Run", run);
                if (blow == false) // chi attack 1 lan trong pham vi dis<= disToStop
                {
                    Invoke("BlowTheBomb", 0.25f);
                    ani.SetTrigger("Blow");
                    blow = true;
                    Debug.Log("L" + blow);
                }
                
            }
            else
            {
                stopMove = false;
                blow = false;// reset
            }

            if (rayBombLeft == true && rayBombLeft.collider.GetComponent<Bomb>().bombOn == true)
            {
                if (stopMove == false)
                {
                    run = true;
                    ani.SetBool("Run", run);
                    rb.transform.position = Vector2.MoveTowards(transform.position, new Vector2(rayBombLeft.collider.transform.position.x + checkPos, transform.position.y), runSpeed * Time.deltaTime);// chi di chuyen ve toa do x, toa do y giu nguyen
                    FaceLeft(true);
                }
                else
                {
                    run = false;
                    ani.SetBool("Run", run);
                }
            }
            else
            {
                run = false;
                ani.SetBool("Run", run);
            }
        }
        else
        {
            dis = disToStop + 1f;
            
        }

    }

    void BlowTheBomb()
    {
        if(rayBombRight.collider!=null && rayBombRight.collider.GetComponent<Bomb>().bombOn==true)
        {
            rayBombRight.collider.GetComponent<Bomb>().BombOff();
            if(IsInvoking()==false)
            {
                Invoke("SetTrueFollowPlayer", 0.5f);
            }
        }
        else if(rayBombLeft.collider != null && rayBombLeft.collider.GetComponent<Bomb>().bombOn == true)
        {
            rayBombLeft.collider.GetComponent<Bomb>().BombOff();
            if (IsInvoking() == false)
            {
                Invoke("SetTrueFollowPlayer", 0.5f);
            }
        }
    }

    void SetTrueFollowPlayer() // viet ham rieng de con Invoke
    {
        followPlayer = true;
        blow = false;
    }

    bool upEnemydie = false;
    public void SetDieEnemy()
    {
        die = true;
        collWhenAlive.enabled = false;
        collWhenDie.enabled = true;
        ani.SetBool("Die", die);
        if(upEnemydie==false) // chi tang so luong enemy die 1 lan
        {
            GameManager.singleton.UpNumberOfEnemyDie();
            upEnemydie = true;
        }
        Invoke("SetActiverFalse", 3f);
    }
    void SetActiverFalse()
    {
        gameObject.SetActive(false);
    }
}

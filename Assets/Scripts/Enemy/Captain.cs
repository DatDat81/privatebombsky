﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Captain : MonoBehaviour
{
    [Header("Mode")]
    public bool priotityPlayer;

    [Header("Property")]
    public Rigidbody2D rb;
    public Animator ani;
    public float runSpeed;
    private bool die = false;
    bool faceRight = true;
    public CapsuleCollider2D collWhenAlive;
    public CapsuleCollider2D collWhenDie;

    [Header("Find Player")]
    public Transform rayPos;
    public float distanceToPlayer;
    public LayerMask whatIsPlayer;
    public float checkPos;
    public float disToStop; // disToStop luon phai lon hon checkPos
    RaycastHit2D collRightPlayer;
    RaycastHit2D collLeftPlayer;

    [Header("Attack Player")]
    public Transform attackPos;
    public float forceAttack;

    [Header("Avoid Bom")]
    public LayerMask whatIsBomb;
    private RaycastHit2D collBomRight;
    private RaycastHit2D collBomLeft;
    public float distanceToAvoid;
    public float checkBombPos;
    public float speedScareRun;

    void Start()
    {
        
    }


    void Update()
    {
        FaceRight(faceRight);

        if(priotityPlayer==false) // uu tien tranh bom truoc
        {
            AvoidBomb();
            if (collBomLeft.collider == null && collBomRight.collider == null)
            {
                FindPlayer();
            }
        }
        else // uu tien danh player truoc
        {
            FindPlayer();          
            if (collRightPlayer.collider == null && collLeftPlayer.collider == null)
            {
                AvoidBomb();
            }
        }
    }

    public void FaceRight(bool right)
    {
        Vector2 local = this.transform.localScale;
        if (right == true)
        {
            local.x = 1;
        }
        else
        {
            local.x = -1;
        }
        this.transform.localScale = local;
    }

    /// <summary>
    /// //////////////////////////////////////////////////PLAYER//////////////////////////////////////////////////////////////////////////////////
    /// </summary>

    void FindPlayer()
    {
        collRightPlayer = Physics2D.Raycast(rayPos.position, Vector2.right, distanceToPlayer, whatIsPlayer);
        collLeftPlayer = Physics2D.Raycast(rayPos.position, Vector2.left, distanceToPlayer, whatIsPlayer);

        if (collRightPlayer.collider != null && die == false && Player.singleton.GetDiePlayer() == false)
        {
            bool isDestination = false; // check xem da den dien de tan cong player chua
            bool attacked = false;
            float des = Vector2.Distance(transform.position, collRightPlayer.collider.transform.position);
            if (des <= disToStop)
            {
                faceRight = true;
                isDestination = true;
                ani.SetBool("Run", false);
                if(attacked==false)
                {
                    attacked = true;
                    ani.SetBool("Attack", attacked);
                    Invoke("Attack", 0.2f);
                }
            }
            else
            {
                attacked = false;
                ani.SetBool("Attack", attacked);
                isDestination = false;
            }

            if (isDestination == false)
            {
                faceRight = true;
                ani.SetBool("Run", true);
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(collRightPlayer.collider.transform.position.x - checkPos, transform.position.y), runSpeed * Time.deltaTime);
            }
        }
        else if (collLeftPlayer.collider != null && die == false && Player.singleton.GetDiePlayer() == false)
        {
            bool isDestination = false; // check xem da den dien de tan cong player chua
            bool attacked = false;
            float des = Vector2.Distance(transform.position, collLeftPlayer.collider.transform.position);
            if (des <= disToStop)
            {
                faceRight = false;
                isDestination = true;
                ani.SetBool("Run", false);
                if (attacked == false)
                {
                    attacked = true;
                    ani.SetBool("Attack", attacked);
                    Invoke("Attack", 0.2f);
                }
            }
            else
            {
                attacked = false;
                ani.SetBool("Attack", attacked);
                isDestination = false;
            }

            if (isDestination == false)
            {
                faceRight = false;
                ani.SetBool("Run", true);
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(collLeftPlayer.collider.transform.position.x + checkPos, transform.position.y), runSpeed * Time.deltaTime);
            }
        }
        else
        { 
            ani.SetBool("Run", false);
        }
    }

    void Attack()
    {
        Collider2D coll = Physics2D.OverlapCircle(attackPos.position, 0.5f, whatIsPlayer);
        if(coll!=null)
        {
            if(faceRight==true)
            {
                //Player.singleton.SetDiePlayer();
                coll.GetComponent<Player>().HitPlayer();
                GameManager.singleton.ShakeCamera();
                coll.GetComponent<Rigidbody2D>().velocity = new Vector2(1, 1) * forceAttack;                
            }
            else
            {
                //Player.singleton.SetDiePlayer();
                coll.GetComponent<Player>().HitPlayer();
                GameManager.singleton.ShakeCamera();
                coll.GetComponent<Rigidbody2D>().velocity = new Vector2(-1, 1) * forceAttack;
            }
        }
    }
    /// <summary>
    /// ////////////////////////////////////////////////////////BOMB////////////////////////////////////////////////////
    /// </summary>
    bool scareRunSound = false;
    void AvoidBomb()
    {
        collBomLeft = Physics2D.Raycast(rayPos.position, Vector2.left, distanceToPlayer, whatIsBomb);
        collBomRight = Physics2D.Raycast(rayPos.position, Vector2.right, distanceToPlayer, whatIsBomb);

        if(collBomRight.collider!=null && die==false && collBomRight.collider.GetComponent<Bomb>().bombOn==true)
        {
            ani.SetBool("Run", false);
            bool isDestination = true;
            float distance = Mathf.Abs(collBomRight.collider.transform.position.x - transform.position.x);
            if(distance<= distanceToAvoid)
            {
                isDestination = false;
                
            }
            else
            {
                isDestination = true;
                ani.SetBool("ScareRun", false);
            }
            if(isDestination==false)
            {
                if(scareRunSound==false) // chi phat am thanh 1 lan k bi lap
                {
                    SoundManager.singleton.CaptainScareRunSound();
                    scareRunSound = true;
                }
                ani.SetBool("ScareRun", true);
                faceRight = false;
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(collBomRight.collider.transform.position.x + checkBombPos, transform.position.y), -speedScareRun * Time.deltaTime);
            }
        }
        else if(collBomLeft.collider != null && die == false && collBomLeft.collider.GetComponent<Bomb>().bombOn == true)
        {
            ani.SetBool("Run", false);
            bool isDestination = true;
            float distance = Mathf.Abs(collBomLeft.collider.transform.position.x - transform.position.x);
            if (distance <= distanceToAvoid)
            {
                isDestination = false;

            }
            else
            {
                isDestination = true;
                ani.SetBool("ScareRun", false);
            }
            if (isDestination == false)
            {
                if (scareRunSound == false) // chi phat am thanh 1 lan k bi lap
                {
                    SoundManager.singleton.CaptainScareRunSound();
                    scareRunSound = true;
                }
                ani.SetBool("ScareRun", true);
                faceRight = true;
                transform.position = Vector2.MoveTowards(transform.position, new Vector2(collBomLeft.collider.transform.position.x - checkBombPos, transform.position.y), -speedScareRun * Time.deltaTime);
            }
        }
        else
        {
            scareRunSound = false;
            ani.SetBool("ScareRun", false);
        }
    }
    bool upEnemyDie = false;
    public void SetDieEnemy()
    {
        die = true;
        ani.SetBool("Die", die);
        collWhenAlive.enabled = false;
        collWhenDie.enabled = true;
        SoundManager.singleton.CaptainDieSound();
        if (upEnemyDie == false) // chi tang so luong enemy die 1 lan
        {
            GameManager.singleton.UpNumberOfEnemyDie();
            upEnemyDie = true;
        }
        Invoke("SetActiverFalse", 2.5f);
    }
    void SetActiverFalse()
    {
        gameObject.SetActive(false);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BOSS1 : MonoBehaviour
{
    public Rigidbody2D rb;

    [Header("Animation")]
    public Animator ani;
    public bool crounch;
    public bool flyingKick;
    public bool hurt;
    public bool kick;
    public bool punch;

    [Header("Transform")]
    public Transform centerPos;

    [Header("Crounch")]
    public bool do_crounch;
    public List<GameObject> fireball_Crounch;

    [Header("Punch")]
    public bool do_punch;
    public GameObject fireball_Punch;

    [Header("Flying Kick")]
    public Transform leftLimit;
    public Transform rightLimit;
    public float speed;
    bool bossInRight = true;
    bool doneFlyingKick=false; // xong toanf booj quas trinhf
    bool doneFlyingKickLeft=false; // kieemr tra đã thwucj hiện xong flying kick sang bên trái hay chưa
    

    [Header("Find Player")]
    public LayerMask whatIsPlayer;
    public float distance;
    RaycastHit2D rayRight;
    RaycastHit2D rayLeft;

    private void Start()
    {
        FaceRight(false);
    }

    private void Update()
    {
        if(do_crounch)
        {
            CrounchMode();
            if(IsInvoking("Set_Fireball_Crounch")==false)
            {
                Invoke("Set_Fireball_Crounch",2f);
            }
        }

        if(do_punch)
        {
            Punch();
            if (IsInvoking("Set_Fireball_Punch") == false)
            {
                Invoke("Set_Fireball_Punch", 2f);
            }
        }

        //rayRight = Physics2D.Raycast(centerPos.position, Vector2.right, distance, whatIsPlayer);
        //rayLeft = Physics2D.Raycast(centerPos.position, Vector2.left, distance, whatIsPlayer);
        //if(rayLeft.collider!=null)
        //{
        //    if(doneFlyingKick==false)
        //    {
        //        FlyingKickLeft();
        //    }
        //}
        //FlyingKickRight();
    }

    public void CrounchMode()
    {
        ani.SetTrigger("Crounch");
        
        for(int i=0;i<fireball_Crounch.Count;i++)
        {
            fireball_Crounch[i].SetActive(true);
        }
    }

    void Set_Fireball_Crounch()
    {
        for (int i = 0; i < fireball_Crounch.Count; i++)
        {
            fireball_Crounch[i].SetActive(false);
            fireball_Crounch[i].transform.position = centerPos.position;
        }
        
    }

    void Set_Fireball_Punch()
    {
        fireball_Punch.SetActive(false);
        fireball_Punch.transform.position = centerPos.position;
    }

    

    public void FlyingKickLeft()
    {       
        if(bossInRight==true)
        {
            FaceRight(false);
            ani.SetBool("FlyingKick", true);
            rb.transform.position = Vector2.MoveTowards(transform.position, leftLimit.position, speed*Time.deltaTime);
            if(Vector2.Distance(transform.position,leftLimit.position)<=0.1)
            {
                ani.SetBool("FlyingKick", false);
                Invoke("SetBossInRight", 1f);
            }
        }
    }
    public void FlyingKickRight()
    {
        if(bossInRight==false&& doneFlyingKickLeft==true)
        {
            FaceRight(true);
            ani.SetBool("FlyingKick", true);
            rb.transform.position = Vector2.MoveTowards(transform.position, rightLimit.position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, rightLimit.position) <= 0.1)
            {
                ani.SetBool("FlyingKick", false);
                //Invoke("SetBossInRight", 1f);
                doneFlyingKick = true;
                FaceRight(false);
            }
        }
    }

    public void Punch()
    {
        ani.SetTrigger("Punch");
        fireball_Punch.SetActive(true);
    }

    void SetBossInRight()
    {
        bossInRight = false;
        doneFlyingKickLeft = true;
    }

    public void FaceRight(bool right)
    {
        Vector2 vt = transform.localScale;
        if(right==true)
        {
            vt.x = 1;
            transform.localScale = vt;
        }
        else
        {
            vt.x = -1;
            transform.localScale = vt;
        }
    }
}

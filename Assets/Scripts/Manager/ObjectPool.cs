﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : Singleton<ObjectPool>
{
    public GameObject bomb;
    public GameObject explosion;
    public GameObject cannonBall;
    public int numberProperty;
    public int numberOfBomb;
    private List<GameObject> listBomb;
    private List<GameObject> listExplosion;
    private List<GameObject> listCannonBall;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        listBomb = new List<GameObject>();
        listExplosion = new List<GameObject>();
        listCannonBall = new List<GameObject>();

        for (int i=0; i < numberOfBomb;i++)
        {
            GameObject obj = Instantiate(bomb, transform.position, transform.rotation);
            obj.SetActive(false);
            obj.transform.parent = transform;
            listBomb.Add(obj);
        }
        for(int i=0;i<numberProperty;i++)
        {
           
            //explotion
            GameObject exp = Instantiate(explosion, transform.position, transform.rotation);
            exp.SetActive(false);
            exp.transform.parent = transform;
            listExplosion.Add(exp);

            //cannonball
            GameObject cannonball = Instantiate(cannonBall, transform.position, transform.rotation);
            cannonball.SetActive(false);
            cannonball.transform.parent = transform;
            listCannonBall.Add(cannonball);
        }
    }

    public GameObject GetBomb()
    {
        for (int i = 0; i < numberProperty; i++)
        {
            if(listBomb[i].activeSelf==false)
            {
                return listBomb[i];
            }
        }
        return null;
    }

    public GameObject GetExplosion()
    {
        for (int i = 0; i < numberProperty; i++)
        {
            if (listExplosion[i].activeSelf == false)
            {
                return listExplosion[i];
            }
        }
        return null;
    }

    public GameObject GetCannonBall()
    {
        for (int i = 0; i < numberProperty; i++)
        {
            if (listCannonBall[i].activeSelf == false)
            {
                return listCannonBall[i];
            }
        }
        return null;
    }

    public void SetBomActiverFalse()// khi an nyt replaygame thi cannon ball k tu bien mat nen can phai set activer cho no
    {
        for (int i = 0; i < numberProperty; i++)
        {
            listCannonBall[i].SetActive(false);
            listBomb[i].SetActive(false);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Home_Manager : MonoBehaviour
{
    int turnOnMusic = 1;
    public Toggle musicToggle;

    int indexMap=0;
    public List<GameObject> listMap;

    void Awake()
    {
        turnOnMusic = PlayerPrefs.GetInt("MusicOn");
        if (turnOnMusic == 1)
            musicToggle.isOn = true;
        else if (turnOnMusic == 0)
            musicToggle.isOn = false;
    }

    private void Update()
    {
    }

    public void MusicToggle()
    {
        Debug.Log("change");
        if (turnOnMusic == 1)
            turnOnMusic = 0;
        else if (turnOnMusic == 0)
            turnOnMusic = 1;
        PlayerPrefs.SetInt("MusicOn", turnOnMusic);
        PlayerPrefs.Save();
    }

    public void NextButton()
    {
        if(indexMap>=0&&indexMap<listMap.Count-1)
        {
            listMap[indexMap].SetActive(false);
            indexMap++;
            listMap[indexMap].SetActive(true);
        }
    }

    public void BackButton()
    {
        if (indexMap > 0 && indexMap < listMap.Count)
        {
            listMap[indexMap].SetActive(false);
            indexMap--;
            listMap[indexMap].SetActive(true);
        }
    }


}

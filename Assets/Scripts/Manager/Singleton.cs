﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour 
{
    public static T result;
    public static T singleton
    {
        get
        {
            return result;
        }
    }

    private void OnEnable()
    {
        if (result == null)
            result = this as T;
    }
}

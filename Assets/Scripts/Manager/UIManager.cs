﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class UIManager : Singleton<UIManager>
{
    [Header("Property")]
    private int numOfBomb;
    public TextMeshProUGUI bomText;
    public TextMeshProUGUI enemyText;

    [Header("Win Game")]
    public GameObject winGame;

    [Header("Lose Game")]
    public GameObject loseGame;

    [Header("Player Controller")]
    public GameObject playerController;

    [Header("Health")]
    public List<GameObject> health;

    private void Start()
    {

    }

    private void Update()
    {
        bomText.text = ""+ GameManager.singleton.GetNumberOfBomb();
        enemyText.text = "" + GameManager.singleton.GetNumberOfEnemy();
    }

    public void HomeButton()
    {
        ObjectPool.singleton.SetBomActiverFalse();
        SceneManager.LoadScene("Home");
    }

    public void NextMap()
    {
        ObjectPool.singleton.SetBomActiverFalse();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void Restart()
    {
        ObjectPool.singleton.SetBomActiverFalse();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    /// <summary>
    /// ////////////////////////
    /// </summary>

    public void SetWinGameTrue()
    {
        winGame.SetActive(true);
        playerController.SetActive(false);
        Player.singleton.joystick.Horizontal = 0;
    }

    public void SetLoseGameTrue()
    {
        loseGame.SetActive(true);
        playerController.SetActive(false);
        Player.singleton.joystick.Horizontal = 0;
    }

    public void RebornButton() // chuyển chức năng Reward video Ad+ reset lại số bom
    {
        Player.singleton.Reborn();
        playerController.SetActive(true);
        loseGame.SetActive(false);
    }

    ///// Health
    public void SetHealth(int index)
    {
        for(int i=0;i<index && i<3;i++) // max co 3 health
        {
            health[i].SetActive(true);
        }
        for(int i=index;i<3;i++)
        {
            health[i].SetActive(false);
        }
    }
}

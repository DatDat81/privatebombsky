﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{
    [Header("Button")]
    public AudioSource moveLeftRightButtonSound;
    public AudioSource jumpButtonSound;
    public AudioSource bomUpButtonSound;
    [Header("Explosion")]
    public AudioSource ExplosionSound;

    [Header("Player")]
    public AudioSource playerDieSound;
    public AudioSource playerHitSound;

    [Header("Bald Pirate")]
    public AudioSource baldPirateAttackSound;
    public AudioSource baldPirateDeathSound;

    [Header("Captain")]
    public AudioSource captainScareRunSound;
    public AudioSource captainDieSound;

    [Header("Whale")]
    public AudioSource whaleSwallowBomSound;
    public AudioSource whaleDieSound;

    [Header("Item")]
    public AudioSource HeartItem;

    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }
    /// <summary>
    /// ////////player
    /// </summary>
    public void PlayMoveLeftRightSound()
    {
        if(PlayerPrefs.GetInt("MusicOn")==1)
        {
            moveLeftRightButtonSound.Play();
        }
        
    }
    public void PlayJumpButtonSound()
    {
        if (PlayerPrefs.GetInt("MusicOn") == 1 ||PlayerPrefs.HasKey("MusicOn")==false)
        {
            jumpButtonSound.Play();
        }
        
    }
    public void PlayerDieSound()
    {
        if (PlayerPrefs.GetInt("MusicOn") == 1 || PlayerPrefs.HasKey("MusicOn") == false)
        {
            playerDieSound.Play();
        }

    }

    public void PlayerHitSound()
    {
        if (PlayerPrefs.GetInt("MusicOn") == 1 || PlayerPrefs.HasKey("MusicOn") == false)
        {
            playerHitSound.Play();
        }

    }

    // button
    public void PlayBomButtonSound()
    {
        if (PlayerPrefs.GetInt("MusicOn") == 1 || PlayerPrefs.HasKey("MusicOn") == false)
        {
            bomUpButtonSound.Play();
        }
        
    }

    public void PlayExplosionSound()
    {
        if (PlayerPrefs.GetInt("MusicOn") == 1 || PlayerPrefs.HasKey("MusicOn") == false)
        {
            ExplosionSound.Play();
        }
        
    }

    ///// Bald Pirate
    public void BaldPirateAttackSound()
    {
        if (PlayerPrefs.GetInt("MusicOn") == 1 || PlayerPrefs.HasKey("MusicOn") == false)
        {
            baldPirateAttackSound.Play();
        }
        
    }

    public void BaldPirateDeathSound()
    {
        if (PlayerPrefs.GetInt("MusicOn") == 1 || PlayerPrefs.HasKey("MusicOn") == false)
        {
            baldPirateDeathSound.Play();
        }
        
    }

    //////////// captain
    public void CaptainScareRunSound()
    {
        if (PlayerPrefs.GetInt("MusicOn") == 1 || PlayerPrefs.HasKey("MusicOn") == false)
        {
            captainScareRunSound.Play();
        }      
    }

    public void CaptainDieSound()
    {
        if (PlayerPrefs.GetInt("MusicOn") == 1 || PlayerPrefs.HasKey("MusicOn") == false)
        {
            captainDieSound.Play();
        }
    }

    /// Whale
    /// 

    public void WhaleSwalowBomSound()
    {
        if (PlayerPrefs.GetInt("MusicOn") == 1 || PlayerPrefs.HasKey("MusicOn") == false)
        {
            whaleSwallowBomSound.Play();
        }
    }

    public void WhaleDieSound()
    {
        if (PlayerPrefs.GetInt("MusicOn") == 1 || PlayerPrefs.HasKey("MusicOn") == false)
        {
            whaleDieSound.Play();
        }
    }

    // item
    public void HeartItemSound()
    {
        if (PlayerPrefs.GetInt("MusicOn") == 1 || PlayerPrefs.HasKey("MusicOn") == false)
        {
            HeartItem.Play();
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllButton : MonoBehaviour
{
    //public void Move(int index)
    //{
    //    if(index==0)
    //    {
    //        Player.singleton.isMovingLeft = true; 

    //    }
    //    else if(index ==1)
    //    {
    //        Player.singleton.isMovingRight = true;
    //    }
    //}

    //public void StopMove()
    //{
    //    Player.singleton.isMovingLeft = false;
    //    Player.singleton.isMovingRight = false;
    //    Player.singleton.StopMove();
    //}

    public void Jump()
    {
        Player.singleton.Jump();
        SoundManager.singleton.PlayJumpButtonSound();
    }

    public void BombDown()
    {
        if(Player.singleton.GetDiePlayer()==false && GameManager.singleton.GetNumberOfBomb() > 0)
            Player.singleton.BombButtonDown();
    }

    public void BombUp()
    {
        if (Player.singleton.GetDiePlayer() == false && GameManager.singleton.GetNumberOfBomb()>0)
        {
            Player.singleton.BombButtonUp();
            GameManager.singleton.SubtractBom(1);
            SoundManager.singleton.PlayBomButtonSound();
        }
        
    }

}

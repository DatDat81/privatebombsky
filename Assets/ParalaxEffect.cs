﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParalaxEffect : MonoBehaviour
{
    float length, startPosX, startPosY;
    public GameObject camera;
    public float paralaxEffect;
    public bool movex;
    public bool movey;
    void Start()
    {
        startPosX = transform.position.x;
        startPosY = transform.position.y;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void Update()
    {
        float distx = camera.transform.position.x * paralaxEffect;
        float disty = camera.transform.position.y * paralaxEffect;
        if(movex==true && movey==true)
        {
            transform.position = new Vector2(startPosX+ distx,startPosY+ disty);
        }
        else if(movex==true && movey==false)
        {
            transform.position = new Vector2(startPosX+ distx, transform.position.y);
            float temp = camera.transform.position.x * (1 - paralaxEffect);
            if (temp > length + startPosX) startPosX += length;
            else if (temp < startPosX - length) startPosX -= length;
            Debug.Log("ha");
        }
        else if(movex==false && movey==true)
        {
            transform.position = new Vector2(transform.position.x,startPosY+ disty);
        }
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConveyorBelt : MonoBehaviour
{
    int index = 0;
    public List<Transform> pos;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.MoveTowards(this.transform.position, pos[index].position, 1 * Time.deltaTime);
        if(Vector2.Distance(transform.position,pos[index].position)<=0.2f)
        {
            if (index == 0)
                index = 1;
            else index = 0;
        }
    }

    //private void OnCollisionEnter2D(Collision2D coll)
    //{
    //    if (coll.collider.tag == "Player")
    //        coll.transform.parent = this.transform;
    //}
    //private void OnCollisionExit2D(Collision2D coll)
    //{
    //    coll.transform.parent = null;
    //}
}

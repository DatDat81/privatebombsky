﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeModeCamera : MonoBehaviour
{
    public bool cameraFollowMode;
    [Header("ModeX")]
    public bool modeFollow_X;
    public float y_position;
    [Header("ModeY")]
    public bool modeFollow_Y;
    public float x_position;

    private void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.tag=="Player")
        {
            if(cameraFollowMode==true)
            {
                Player.singleton.CameraFollowMode();
            }else if(modeFollow_X==true)
            {
                Player.singleton.CameraFollow_X_Mode(y_position);
            }
            else if(modeFollow_Y==true)
            {
                Player.singleton.CameraFollow_Y_Mode(x_position);
            }
        }
    }
}
